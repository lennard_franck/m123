[TOC]
# DNS

## Auftrag 1 DNS-Server auf Windows
In diesem Auftrag werde ich euch zeigen vie man einen DNS- Server auf einer windows vm aufsetzt.

1. Man installiert sich das passende iso file auf "azure" Ich habe den Windows Standart server ausgesucht.
![Bild17](./Images/bild18.png)

2. Dancach erstellt mkan eine neue VM (ich habs mit VMware workstation gemacht)
3. Wichtig ist, das man das Herutergeladene iso file einsetzt.

4. Danach kommt das installieren, des Servers

5. Es wird nach einem Key gefragt, unter Azure, kann man sich den Key anzeigen lassen.

6. Das wichtigste ist, das man hier das richtige auswählt. Wenn nicht die Desktopdarstellung nimmt, öffnet sich nur das Terminal.
![bild19](./Images/bild19.png)
7. Danach wird der Server instaliert.
8. Wenn man den Server richtig instaliert, öffnet sich automatisch der Server- Manager.
![bild20](./Images/bild20.png)

### DNS-Konfiguration

1. Als erstes müssen wir die Rollen unt Feautures hinzufügen.
![bild21](./Images/bild21.png)

2. Hier habe ich die Voreinstellungen übernommen.

![bild22](./Images/bild22.png)

Jetz wird werden die tools Instaliert.

3. Als nächstes müssen wir unsere Zone erstellen, das macht man unter "Tools"
![bild23](./Images/bild23.png)

Wir benötigen nur Forward und Reverse.

4. Mit rchtsklick auf den Server kann man eine neue Zone erstellen.

5. Es wird nach der netzwerk Id gefragt. Hierfür habe ich das Netzwerk aus dem Modul 117. 

![Bild24](./Images/bild24.png)

6. Nun hast du deine Zone erstellt.

7. Als nächstes werden ich Namen zu Ip- Adressen machen.
8. Dies macht man, indem man rechtsklich Die Zone macht. ![Bild25](./Images/bild25.png)

### Records
 Wie man auf dem Bild unten sieht habe ich beide records erstellt. Die Ip-adressen kann man frei wählen.
 ![bild26](./Images/bild.26.png)

Mit einem Rechtsklick auf dem Servernamen und dann unter Eigenschaften gehen, kann man den 
Forwarder einrichten. Dort werden die Anfragen für den DNS-Server weitergeleitet, wenn dieser 
keine Informationen austeilen kann. Der Forwarder ist ein Server meistens ausserhalb des 
Netzwerkes, dns.google ist meistens vertreten. Im Bild auf der nächsten Seite sieht man das Fenster 
zum Hinzufügen.

 ![bild27](./Images/bild28.png)

 8.8.8.8 ist der Google-dns server (der Standart)

 Jetzt sollte alles richtig Konfiguriert sein.

 Das kann man prüfen unter cmd mit "nslookup"

Hier ist der Beweis: 

![Bild29](./Images/bild29.png)
 

 