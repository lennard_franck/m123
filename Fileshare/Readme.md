[TOC]
# Samba


# Auftrag 1 Samba Share

In diesem Auftrag werde ich das Samba paket installieren.

 Am wichtigsten ist das man den Server oder Desktop installiert und nicht nur testet!! 

Als erstes habe ich alles auf den neusten Stand gemacht, dies macht man mit *"sudo apt update"* 


![bild30](./Images/bild%2030.png)

- Check its service is active and running:

*"systemctl status smbd --no-pager -l"*
![bild31](./Images/bild31.png)

- To make the service enabled to start automatically with system boot, here is the command:

*"sudo systemctl enable --now smbd"*

gebe diesen Befehl für die Firewall ein:
![bild32](./Images/bild32.png)
 
## Samba Paket Instalieren
Mit dem Befehl *"sudo apt install samba"* kann man lädt man Samba herunter.
## Freigabe Ordner erstellen
Damit man Daten Freigeben kann, muss man ein Ordner erstellen. Dies macht man mit *"sudo mkdir /media/Share"*

Damit jeder auf den freigegebenen Ordner zugreifen kann muss man die Berechtigungen anpassen mit chmod.

 Der Befehl ist: *"sudo chmod 777 /media/Share"*
 Jetzt hat jeder Vollzugriff

## Testen/Beweisen

Wenn man einen ordner erstellt, kann man das ganze noch Beweisen.


![bild33](./Images/bild33.png)


## Troubleshooting

Ich kann vom Widows client den die Ubuntu vm nicht pingen.

Ich habe Firewalls ausgeschalten, es geht trotzdem nicht.