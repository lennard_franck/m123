[TOC]


# DHCP
-[verzeichnis]

## Auftrag 1 DHCP in a Nutshell
In diesem Auftrag erkläre ich, wie man ein DCHP server aufsetzt.
 - Mithilfe von KKabeln konnte verbindungen aufbauen:
 
 Server zu  Switch 

 Switch zu Clients 
 
 Danach sah die Topologie so aus:
 ![Bild_Fillius](./Images/SC%20Fillius%20Topologie.png)
 - Cient 3 hat bereits eine Fixe Ip- adresse zu geteil. Hier die Anleitung:
 
 linkslick auf den DHCP- server -> unten rechts auf DHCP- Server einrichten -> unter Statische Adressierung kann man die Vorgegebene MAC- und IP- Adresse festlegen.
 - Clients 1 und 2 haben keine fixe Ip- adressen und müssen durch den DHCP- Server IP- adressen zugeteil bekommen. 
 - wenn man den DHCP- Server richtig Konfiguriert hat kann man dieses Resultat sehen:

 
![Bild Fillius](./Images/SC%20Bweisfoto%20Filius.png)
.

## Auftrag 2 

In diesem Auftrag werde ich zeigen wie ich den ![Auftrag](https://gitlab.com/alptbz/m123/-/blob/main/05_DHCP/DHCP_PacketTracer/01_DHCP.md?ref_type=heads) gelöst habe.


Als erstes Muss man überprüfen in welchem Subnetz und in Welchem Range der DHCP- Server IP-andressen verteilt.
Die macht man, indem man auf dem Router in enable modus geht und diese Befehle eingibt:

- show ip dhcp pool

- show ip dhcp binding

- show running-config

Mit diesen Befehlen kannn man dann folgenden Fragen beantworten.

- Für welches Subnetz ist der DHCP Server aktiv?

Der Server ist für das /24 Netzwerk 
- Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)

| Ip-Adresse    | Mac- Adresse   |
|---------------|----------------|
| 192.168.35.26 | 00E0.8F4E.65AA |
| 192.168.35.27 | 0007.ECB6.4534 |
| 192.168.35.23 | 0009.7CA6.4CE3 |
| 192.168.35.25 | 0050.0F4E.1D82 |
| 192.168.35.24 | 0001.632C.3508 |
| 192.168.35.28 | 00D0.BC52.B29B |


- In welchem Range vergibt der DHCP-Server IPv4 Adressen?

Der Server vergibt IPv4 Adressen im Range von 192.168.35.1 - 192.168.35.254

- Was hat die Konfiguration ip dhcp excluded-address zur Folge?

Das der DHCP- Server keine Adressen im Range von 192.168.35.1 - 192.168.35.22
und 192.168.35.146 - 192.168.35.230 vergeben kann. Die Adressen in diesem Range können nicht Automatisch vergeben werden.


- Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?


# DORA - DHCP Lease beobachten

Mit dem Cisco Packet Tracer können Ethernet Frames oder allgemeiner Protocol Data Unit (PDU) sehr gut nachverfolgt werden. In diesem Schritt soll eine DHCP IPv4-Adress-Zuteilvorgang mitverfolgt werden.


Wenn man die Anleitung genau befolgt kann man genau beobachten, was der DHCP- Server macht. 

Fragen: 

- Welcher Op-Code hat der DHCP-Discover?

DHCP: Op Code (op) = 1 (0x1)
![Bild Cisco](./Images/bild4.png/)

- Welcher OP-Code hat der DHCP-Offer?

DHCP: Op Code (op) = 2 (0x2)
![Bild Cisco](./Images/bild5.png)
- Welcher OP-Code hat der DHCP-Request?

DHCP: Op Code (op) = 1 (0x1)
![Bild](./Images/bild6.png)

- Welcher OP-Code hat der DHCP-Acknowledge?


- An welche IP-Adresse wird der DHCP-Discover geschickt? 

255.255.255.255
![Bild](./Images/Bild7.png)

- Was ist an dieser IP-Adresse speziell?

Es ist eine Broadcast Adresse.
- An welche MAC-Adresse wird der DHCP-Discover geschickt?
Mac: FFFF.FFFF.FFFF
-  Was ist an dieser MAC-Adresse speziell?

Das ist die die Broadcastadresse. Das braucht sie um den DHCP-Server zu erreichen.
- Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
Discover wird immer als Broadcast geschickt. Er hat keine genaue Zieladresse.
- Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?
Ja. Der Switch ist das DHCP-Relay Gerät.



## Netzwerk umkonfigurieren

Ich habe dem Server die 192.168.35.30 IPv4- Adresse geben.
![bild](./Images/bild8.png)

In diesem Range vergibt der DHCP- Server Ip- Adressen:

![Cisco Bild](./Images/bild9.png)


Wenn man alles richtig gemacht hat, kann man nun einen belibigen bin machen und mit PC's im gleichen Netzwerk kommunizieren.


# DHCP Dienst zur Verfügung stellen

Damit wir DHCP- server konfigurieren können müssen wir ein Linux Betriebssystem aufsetzen. Ich habe es so weit geschafft:

- 1. Das ist mein Startbildschirm. Ich habe "try or intall unbuntu server" ausgewählt.
![bild10](./Images/bild10.png)


- 2. Danach habe ich alle Anfragen durchgeklickt, bis zu dem Punkt wo ich ein Konto erstellen konnte.

- 3. Hier habe ich mir ein account erstellt: 
![bild11](./Images/bild11.png)

- 4. Mein Betriebssystem musste nun installiert werden das dauerte einige Minuten
![bild12](./Images/bild12.png)

- 5. Nach einigen Mintuen poppte dieses Fenster auf:
![bild13](./Images/bild13.png)
hier musste ich mich einloggen.


- 6. Als ich mich Eingellogt habe sah es so aus und ich konnte mit den Befehlen beginnen.
![bild14](./Images/bild14.png)

## Installation des Servers via Terminal

 1. DHCP- Dienst herunterladen: "sudo apt install isc-dhcp-server"
 
 2. Überprüfung des Status des Dienstes: "sudo systemctl status isc-dhcp-server"

![bild15](./Images/bild15.png)
nach beiden befehlen sieht es so aus.

3. Backup der Config- Datei: 
"sudo cp /etc/dhcp/dhcpd.conf  /etc/dhcp/dhcpd.conf.backup"

4. Um gespeicherete Konfigurationsdatei zu sehen gitbt man diesen Befehl ein:
 sudo nano /etc/dhcp/dhcpd.conf

Die geöffnete Datei sieht so aus:
![bild16](./Images/bild16.png)

5. Wenn man in dieser Datei ein bisschen herunterscrollt bis zu dieser Stelle wo die Subnetz- Ip und die Netzmaske sind kann man sehen das, die Subnetz-IP 192.168.100.0 ist  und Netzmaske 255.255.255.0.

## Troubleshoot

Jetz will ich alles Konfigurien. Es geht aber nichts mehr. Immer wenn ich in den editor gehe und meinen Server Konfigurien möchte, kann ich nichts mehr drücken/bewegen. 
Ebenfalls kann ich den editor nicht mehr schliessen. ALLES IST EINGEFROREN!
Ich kann nichtmal die weiteren 111 Zeilen lesen.

Das einzige wo ich machen kann ist die Vm neustarten. 

Wenn man googlet findet man keine hilfreichen Tipps. 
Das findet man:
![bild 16](./Images/bild17.png)

